// -*- coding: utf-8; indent-tabs-mode: nil; mode: java; word-wrap: t -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2018-12-05 22:17:27 roukoru>

/*
  http://adventofcode.com/2018/day/3

--- Day 3: No Matter How You Slice It ---

The Elves managed to locate the chimney-squeeze prototype fabric for Santa's suit (thanks to someone who helpfully wrote its box IDs on the wall of the warehouse in the middle of the night). Unfortunately, anomalies are still affecting them - nobody can even agree on how to cut the fabric.

The whole piece of fabric they're working on is a very large square - at least 1000 inches on each side.

Each Elf has made a claim about which area of fabric would be ideal for Santa's suit. All claims have an ID and consist of a single rectangle with edges parallel to the edges of the fabric. Each claim's rectangle is defined as follows:

    The number of inches between the left edge of the fabric and the left edge of the rectangle.
    The number of inches between the top edge of the fabric and the top edge of the rectangle.
    The width of the rectangle in inches.
    The height of the rectangle in inches.

A claim like #123 @ 3,2: 5x4 means that claim ID 123 specifies a rectangle 3 inches from the left edge, 2 inches from the top edge, 5 inches wide, and 4 inches tall. Visually, it claims the square inches of fabric represented by # (and ignores the square inches of fabric represented by .) in the diagram below:

...........
...........
...#####...
...#####...
...#####...
...#####...
...........
...........
...........

The problem is that many of the claims overlap, causing two or more claims to cover part of the same areas. For example, consider the following claims:

#1 @ 1,3: 4x4
#2 @ 3,1: 4x4
#3 @ 5,5: 2x2

Visually, these claim the following areas:

........
...2222.
...2222.
.11XX22.
.11XX22.
.111133.
.111133.
........

The four square inches marked with X are claimed by both 1 and 2. (Claim 3, while adjacent to the others, does not overlap either of them.)

If the Elves all proceed with their own plans, none of them will have enough fabric. How many square inches of fabric are within two or more claims?

--- Part Two ---

Amidst the chaos, you notice that exactly one claim doesn't overlap by even a single square inch of fabric with any other claim. If you can somehow draw attention to it, maybe the Elves will be able to make Santa's suit after all!

For example, in the claims above, only claim 3 is intact after all claims are made.

What is the ID of the only claim that doesn't overlap?

*/

// package aoc2018;

// import java.io.BufferedReader;
// import java.io.File;
// import java.io.InputStreamReader;
import java.io.IOException;

// import java.lang.Double;
import java.lang.Integer;
// import java.lang.reflect.Method;
// import java.lang.String;
// import java.lang.StringBuilder;

import java.nio.file.FileSystems;

// import java.security.MessageDigest;

// import java.text.DateFormat;

// import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
// import java.util.Collection;
// import java.util.Date;
// import java.util.Deque;
// import java.util.HashMap;
// import java.util.HashSet;
// import java.util.List;
// import java.util.Map;
import java.util.Scanner;
// import java.util.Set;
// import java.util.Vector;
// import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
// import java.util.stream.Collectors;
// import java.util.stream.Stream;

import java.util.logging.Level;
import java.util.logging.Logger;

public class aoc2018_03 implements AoC {

    final boolean __USE_LOG = false;
    //final Level __LOG_LEVEL = Level.FINE;
    final Level __LOG_LEVEL = Level.INFO;
    Logger log;
    final String[] test_input = {
        "#1 @ 1,3: 4x4",
        "#2 @ 3,1: 4x4",
        "#3 @ 5,5: 2x2"
    };
    final String[] data_input = {"data"};

    public void help() {
        System.out.println
            ("java aoc2018 03 input_source\n" +
             "where input_source may be:\n" +
             "test\t common test data from aoc (default, if source not specified)\n" +
             "builtin\t builtin use-specific data from aoc\n"+
             "-\t use stdin\n" +
             "file\t use file");
    }

    public void eval (final String[] args) throws Exception { // main ... a sort of
        log = Logger.getGlobal();
        log.setLevel (__LOG_LEVEL);
        // if (args.length < 1) return;
        // int[][] 任务 = read_input (args[1]);
        try {
            InputData 任务 = read_parse_input (args); // rènwù
            System.out.println (process1 (任务));
            System.out.println (process2 (任务));
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private Scanner get_input_scanner (final String src) throws IOException {
        /**
         * helper for getting correct instance of Scanner for
         * reading lines from:
         * stdin (src="-")
         * or other file
         */
         return src.equals("-") ?
            new Scanner (System.in) :
            new Scanner (FileSystems.getDefault().getPath (src));
    }

    private String[] read_lines (final String src) throws IOException {
        /**
         * read lines from stdin (src="-") or other file
         * #2 @ 3,1: 4x4
         */
        Scanner 输入流 = get_input_scanner (src); // shūrù liú
        ArrayList <String> 矩阵 = new ArrayList <String> (); // jǔzhèn
        for (String 串; 输入流.hasNext();) {
            串 = 输入流.nextLine();
            if (串 != null && 串.length() >= 13)
                矩阵.add (串);
        }
        return 矩阵.toArray (new String [矩阵.size()]);
    }

    private InputData read_parse_input (final String[] args)
        throws Exception, IOException {
        String[] 串川;
        if (args.length < 2)
            串川 = test_input;
        else {
            if (args[1].equals ("test"))
                串川 = test_input;
            else
                if (args[1].equals ("builtin"))
                    串川 = data_input;
                else
                    串川 = read_lines (args[1].equals ("-") ? "-" : args[1]);
        }
        return parse_input (串川);
    }

    private InputData parse_input (final String[] indata) throws Exception {
        /**
         * #2 @ 3,1: 4x4
         */
        InputData 桂马 = new InputData (indata); // gui ma
        ArrayList <ワンピス> 表 = new ArrayList <> ();
        ワンピス 这个;
        for (String 串 : indata) {
            // хотя в целом, это лишнее
            // 这个 = new ワンピス (串);
            这个 = new ワンピス (串);
            if (这个 != null)
                表.add (这个);
        }
        桂马.地 = 表.toArray (new ワンピス[0]);
        return 桂马;
    }

    // todo : rework! get_max_*
    private int get_max_length (final InputData 任务) {
        int 边 = 0, 总;
        for (ワンピス 这个 : 任务.地) {
            总 = 这个.南 + 这个.长;
            if (边 < 总)
                边 = 总;
        }
        return 边;
    }

    private int get_max_width (final InputData 任务) {
        int 边 = 0, 总;
        for (ワンピス 这个 : 任务.地) {
            总 = 这个.东 + 这个.宽;
            if (边 < 总)
                边 = 总;
        }
        return 边;
    }

    private int[][] prepare_field (final InputData 任务) {
        int 宽 = get_max_width (任务),
            长 = get_max_length (任务);
        int[][] 表 = new int [长][];
        int 位;
        for (位 = 0; 位 < 长; ++位) {
            表[位] = new int [宽];
            Arrays.fill (表[位], 0);
        }
        return 表;
    }

    private void mark_piece (final int[][] 表, final ワンピス 片) {
        /**
         * 0 free
         * +1.. claimed only once by ...
         * -1.. claimed more than once
         */
        int 南, 东;
        for (南 = 片.南; 南 < (片.南 + 片.长); ++南)
            for (东 = 片.东; 东 < (片.东 + 片.宽); ++东) {
                if (表[南][东] == 0) {
                    表[南][东] = 片.数;
                    continue;
                }
                if (表[南][东] > 0) {
                    表[南][东] = -1;
                }
            }
    }

    private boolean check_piece (final int[][] 表, final ワンピス 片) {
        /**
         * 0 free
         * +1.. claimed only once by ...
         * -1.. claimed more than once
         */
        int 南, 东;
        for (南 = 片.南; 南 < (片.南 + 片.长); ++南)
            for (东 = 片.东; 东 < (片.东 + 片.宽); ++东)
                if (表[南][东] < 0)
                    return false;
        return true;
    }

    private int process1 (final InputData 任务) {
        /**
         * naive:
         * setup matrix (P,Q)
         * mark on it all claims
         * count intersections
         */
        int 位, 数号 = 0; // shǔ
        int[][] 表 = prepare_field (任务);
        for (ワンピス 片 : 任务.地) // piàn | piān
            mark_piece (表, 片);
        for (int[] 行 : 表) { // háng
            for (位 = 0; 位 < 行.length; ++位)
                if (行[位] < 0)
                    ++数号;
        }
        return 数号;
    }

    private int process2 (final InputData 任务) {
        int[][] 表 = prepare_field (任务);
        for (ワンピス 片 : 任务.地) // piàn | piān
            mark_piece (表, 片);
        for (int 位 = 0; 位 < 任务.地.length; ++位)
            if (check_piece (表, 任务.地[位]))
                return 任务.地[位].数;
        return -1;
    }


    class InputData {
        String[] 表;
        ワンピス[] 地;
        InputData() {表 = null;}
        InputData (final String[] 目录) {表 = Arrays.copyOf (目录, 目录.length);}
        public String toString() {return Arrays.toString (表);}
    }

    private static Pattern 路 = Pattern.compile
            ("^#(\\d+) @ (\\d+),(\\d+): (?<len>\\d+)x(?<wdt>\\d+)$");

    class ワンピス {
        /**
         * #123 @ 3,2: 5x4
         * #123 @ 南,东: 长x宽
         */
        int 南, 东,
            长, 宽, //kuān
            数;

        ワンピス() {
            南 = 0;
            东 = 0;
            长 = 0;
            宽 = 0;
            数 = 0;
        }

        ワンピス (final int 数, final int 南, final int 东, final int 长, final int 宽) {
            this.数 = 数;
            this.南 = 南;
            this.东 = 东;
            this.长 = 长;
            this.宽 = 宽;
        }

        ワンピス (final String 串) throws Exception {
            Matcher 配; // pèi
            配 = 路.matcher (串);
            if (配 == null || !配.matches()) {
                log.info (串);
                throw new Exception();
            }
            数 = Integer.parseInt (配.group (1));
            南 = Integer.parseInt (配.group (2));
            东 = Integer.parseInt (配.group (3));
            长 = Integer.parseInt (配.group ("len"));
            宽 = Integer.parseInt (配.group ("wdt"));
        }

        private int min (int 甲, int 乙) {return 甲 < 乙 ? 甲 : 乙;}
        private int max (int 甲, int 乙) {return 甲 > 乙 ? 甲 : 乙;}

        /*
        public int intersect (final ワンピス 别) { // bié
            if ((南 + 长) <= 别.南 || 南 >= (别.南 + 别.长))
                return 0;
            return 0;
        }
        */
    }

}
