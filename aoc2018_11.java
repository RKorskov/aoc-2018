// -*- coding: utf-8; indent-tabs-mode: nil; mode: java; word-wrap: t -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2018-12-11 15:59:55 roukoru>

/*
  http://adventofcode.com/2018/day/11

--- Day 11: Chronal Charge ---

You watch the Elves and their sleigh fade into the distance as they head toward the North Pole.

Actually, you're the one fading. The falling sensation returns.

The low fuel warning light is illuminated on your wrist-mounted device. Tapping it once causes it to project a hologram of the situation: a 300x300 grid of fuel cells and their current power levels, some negative. You're not sure what negative power means in the context of time travel, but it can't be good.

Each fuel cell has a coordinate ranging from 1 to 300 in both the X (horizontal) and Y (vertical) direction. In X,Y notation, the top-left cell is 1,1, and the top-right cell is 300,1.

The interface lets you select any 3x3 square of fuel cells. To increase your chances of getting to your destination, you decide to choose the 3x3 square with the largest total power.

The power level in a given fuel cell can be found through the following process:

    Find the fuel cell's rack ID, which is its X coordinate plus 10.
    Begin with a power level of the rack ID times the Y coordinate.
    Increase the power level by the value of the grid serial number (your puzzle input).
    Set the power level to itself multiplied by the rack ID.
    Keep only the hundreds digit of the power level (so 12345 becomes 3; numbers with no hundreds digit become 0).
    Subtract 5 from the power level.

For example, to find the power level of the fuel cell at 3,5 in a grid with serial number 8:

    The rack ID is 3 + 10 = 13.
    The power level starts at 13 * 5 = 65.
    Adding the serial number produces 65 + 8 = 73.
    Multiplying by the rack ID produces 73 * 13 = 949.
    The hundreds digit of 949 is 9.
    Subtracting 5 produces 9 - 5 = 4.

So, the power level of this fuel cell is 4.

Here are some more example power levels:

    Fuel cell at  122,79, grid serial number 57: power level -5.
    Fuel cell at 217,196, grid serial number 39: power level  0.
    Fuel cell at 101,153, grid serial number 71: power level  4.

Your goal is to find the 3x3 square which has the largest total power. The square must be entirely within the 300x300 grid. Identify this square using the X,Y coordinate of its top-left fuel cell. For example:

For grid serial number 18, the largest total 3x3 square has a top-left corner of 33,45 (with a total power of 29); these fuel cells appear in the middle of this 5x5 region:

-2  -4   4   4   4
-4   4   4   4  -5
 4   3   3   4  -4
 1   1   2   4  -3
-1   0   2  -5  -2

For grid serial number 42, the largest 3x3 square's top-left is 21,61 (with a total power of 30); they are in the middle of this region:

-3   4   2   2   2
-4   4   3   3   4
-5   3   3   4  -4
 4   3   3   4  -3
 3   3   3  -5  -1

What is the X,Y coordinate of the top-left fuel cell of the 3x3 square with the largest total power?

Your puzzle input is 3214.

-- Part Two ---

You discover a dial on the side of the device; it seems to let you select a square of any size, not just 3x3. Sizes from 1x1 to 300x300 are supported.

Realizing this, you now must find the square of any size with the largest total power. Identify this square by including its size as a third parameter after the top-left coordinate: a 9x9 square with a top-left corner of 3,5 is identified as 3,5,9.

For example:

    For grid serial number 18, the largest total square (with a total power of 113) is 16x16 and has a top-left corner of 90,269, so its identifier is 90,269,16.
    For grid serial number 42, the largest total square (with a total power of 119) is 12x12 and has a top-left corner of 232,251, so its identifier is 232,251,12.

What is the X,Y,size identifier of the square with the largest total power?
*/

// package aoc2018;

// import java.io.BufferedReader;
// import java.io.File;
// import java.io.InputStreamReader;
import java.io.IOException;

// import java.lang.Double;
import java.lang.Integer;
// import java.lang.reflect.Method;
// import java.lang.String;
// import java.lang.StringBuilder;

import java.nio.file.FileSystems;

// import java.security.MessageDigest;

// import java.text.DateFormat;

// import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
// import java.util.Collection;
// import java.util.Date;
// import java.util.Deque;
// import java.util.HashMap;
// import java.util.HashSet;
// import java.util.List;
// import java.util.Map;
import java.util.Scanner;
// import java.util.Set;
// import java.util.Vector;
// import java.util.function.Function;
// import java.util.regex.Matcher;
// import java.util.regex.Pattern;
// import java.util.stream.Collectors;
// import java.util.stream.Stream;

import java.util.logging.Level;
import java.util.logging.Logger;

public class aoc2018_11 implements AoC {

    final boolean __USE_LOG = false;
    //final Level __LOG_LEVEL = Level.FINE;
    final Level __LOG_LEVEL = Level.INFO;
    Logger log;
    final int[][] test_input = {
        {122, 79, 57, -5},
        {217, 196, 39, 0},
        {101, 153, 71, 4}
    };
    final int data_input = 3214;

    public void help() {
        System.out.println
            ("java aoc2018 11 input_source\n" +
             "where input_source may be:\n" +
             "test\t common test data from aoc (default, if source not specified)\n" +
             "builtin\t builtin use-specific data from aoc\n"+
             "-\t use stdin\n" +
             "file\t use file");
    }

    public void eval (final String[] args) throws Exception { // main ... a sort of
        log = Logger.getGlobal();
        log.setLevel (__LOG_LEVEL);
        try {
            int 任务 = read_parse_input (args); // rènwù
            System.out.println (process1 (任务));
            System.out.println (process2 (任务));
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private int read_parse_input (final String[] args)
        throws Exception {
        int 号 = data_input;
        if (args.length < 2)
            do_tests();
        else {
            if (args[1].equals ("test"))
                do_tests();
            else
                if (!args[1].equals ("builtin"))
                    号 = Integer.parseInt (args[1]);
        }
        return 号;
    }

    final static int GSIZE = 300;
    private String process1 (final int 任务) {
        int[][] 格 = fill_grid (任务);
        int 数号 = -0x7FFFFF, 东 = -1, 南 = -1, 力, 行, 列; // háng liè
        for (行 = 0; 行 < (GSIZE - 2); ++行) {
            for (列 = 0; 列 < (GSIZE - 2); ++列) {
                力 = power_square (格, 行, 列);
                if (力 > 数号) {
                    数号 = 力;
                    东 = 列+1;
                    南 = 行+1;
                }
            }
        }
        return 东+","+南;
    }

    private String process2 (final int 任务) {
        return ",";
    }

    private int power_square (final int[][] 格, final int 东, final int 南) {
        int 号 = 格[南][东+0] + 格[南+1][东+0] + 格[南 + 2][东+0] +
                 格[南][东+1] + 格[南+1][东+1] + 格[南 + 2][东+1] +
                 格[南][东+2] + 格[南+1][东+2] + 格[南 + 2][东+2];
        return 号;
    }

    private int[][] fill_grid (final int 任务) {
        int[][] 格 = new int[GSIZE][];
        int 行, 列; // háng liè
        for (行 = 0; 行 < GSIZE; ++行)
            格[行] = new int [GSIZE];
        for (行 = 0; 行 < GSIZE; ++行) {
            for (列 = 0; 列 < GSIZE; ++列)
                格[行][列] = charge (行+1, 列+1, 任务);
            // System.out.println (Arrays.toString (格[行]));
        }
        return 格;
    }

    private void do_tests() {
        for (int[] 行: test_input)
            System.out.printf ("%d %d %d\t%d == %d\n",
                               行[0], 行[1], 行[2], 行[3],
                               charge (行[0], 行[1], 行[2]));
        System.exit (0);
    }

    private int charge (final int 东, final int 南, final int 编号) { // biān hào
        /**
         * Find the fuel cell's rack ID, which is its X coordinate plus 10.
         * Begin with a power level of the rack ID times the Y coordinate.
         * Increase the power level by the value of the grid serial number (your puzzle input).
         * Set the power level to itself multiplied by the rack ID.
         * Keep only the hundreds digit of the power level (so 12345 becomes 3; numbers with no hundreds digit become 0).
         * Subtract 5 from the power level.
         */
        int 架 = 东 + 10;
        int 力 = 架 * 南;
        力 += 编号;
        力 *= 架;
        力 = (力 / 100) % 10;
        力 -= 5;
        return 力;
    }

}
