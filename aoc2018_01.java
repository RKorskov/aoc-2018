// -*- coding: utf-8; indent-tabs-mode: nil; mode: java; word-wrap: t -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2018-12-05 00:16:21 roukoru>

/*
  http://adventofcode.com/2018/day/1

--- Day 1: Chronal Calibration ---

After feeling like you've been falling for a few minutes, you look at the device's tiny screen. "Error: Device must be calibrated before first use. Frequency drift detected. Cannot maintain destination lock." Below the message, the device shows a sequence of changes in frequency (your puzzle input). A value like +6 means the current frequency increases by 6; a value like -3 means the current frequency decreases by 3.

For example, if the device displays frequency changes of +1, -2, +3, +1, then starting from a frequency of zero, the following changes would occur:

    Current frequency  0, change of +1; resulting frequency  1.
    Current frequency  1, change of -2; resulting frequency -1.
    Current frequency -1, change of +3; resulting frequency  2.
    Current frequency  2, change of +1; resulting frequency  3.

In this example, the resulting frequency is 3.

Here are other example situations:

    +1, +1, +1 results in  3
    +1, +1, -2 results in  0
    -1, -2, -3 results in -6

Starting with a frequency of zero, what is the resulting frequency after all of the changes in frequency have been applied?

--- Part Two ---

You notice that the device repeats the same frequency change list over and over. To calibrate the device, you need to find the first frequency it reaches twice.

For example, using the same list of changes above, the device would loop as follows:

    Current frequency  0, change of +1; resulting frequency  1.
    Current frequency  1, change of -2; resulting frequency -1.
    Current frequency -1, change of +3; resulting frequency  2.
    Current frequency  2, change of +1; resulting frequency  3.
    (At this point, the device continues from the start of the list.)
    Current frequency  3, change of +1; resulting frequency  4.
    Current frequency  4, change of -2; resulting frequency  2, which has already been seen.

In this example, the first frequency reached twice is 2. Note that your device might need to repeat its list of frequency changes many times before a duplicate frequency is found, and that duplicates might be found while in the middle of processing the list.

Here are other examples:

    +1, -1 first reaches 0 twice.
    +3, +3, +4, -2, -4 first reaches 10 twice.
    -6, +3, +8, +5, -6 first reaches 5 twice.
    +7, +7, -2, -7, -4 first reaches 14 twice.

What is the first frequency your device reaches twice?

*/

// package aoc2018;

// import java.io.BufferedReader;
// import java.io.File;
// import java.io.InputStreamReader;
import java.io.IOException;

// import java.lang.Double;
import java.lang.Integer;
// import java.lang.reflect.Method;
// import java.lang.String;
// import java.lang.StringBuilder;

import java.nio.file.FileSystems;

// import java.security.MessageDigest;

// import java.text.DateFormat;

// import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
// import java.util.Collection;
// import java.util.Date;
// import java.util.Deque;
// import java.util.HashMap;
import java.util.HashSet;
// import java.util.List;
// import java.util.Map;
import java.util.Scanner;
import java.util.Set;
// import java.util.Vector;
// import java.util.function.Function;
// import java.util.regex.Matcher;
// import java.util.regex.Pattern;
// import java.util.stream.Collectors;
// import java.util.stream.Stream;

import java.util.logging.Level;
import java.util.logging.Logger;

public class aoc2018_01 implements AoC {

    final boolean __USE_LOG = false;
    //final Level __LOG_LEVEL = Level.FINE;
    final Level __LOG_LEVEL = Level.INFO;
    Logger log;
    final String[] test_input = {"test"};
    final String[] data_input = {"data"};

    public void help() {
        System.out.println
            ("java aoc2018 01 input_source\n" +
             "where input_source may be:\n" +
             "test\t common test data from aoc (default, if source not specified)\n" +
             "builtin\t builtin use-specific data from aoc\n"+
             "-\t use stdin\n" +
             "file\t use file");
    }

    public void eval (final String[] args) throws Exception { // main ... a sort of
        log = Logger.getGlobal();
        log.setLevel (__LOG_LEVEL);
        try {
            int[] 任务 = read_input (args.length > 1 ? args[1] : "-"); // rènwù
            System.out.println (process1 (任务));
            System.out.println (process2 (任务));
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private Scanner get_input_scanner (final String src) throws IOException {
        /**
         * helper for getting correct instance of Scanner for
         * reading lines from:
         * stdin (src="-")
         * or other file
         */
         return src.equals("-") ?
            new Scanner (System.in) :
            new Scanner (FileSystems.getDefault().getPath (src));
    }

    private int[] read_input (final String src) throws IOException {
        /**
         * read signed int from src
         */
        Scanner 输入流 = get_input_scanner (src); // shūrù liú
        ArrayList <Integer> 号号 = new ArrayList <> ();
        for (; 输入流.hasNextInt();)
            号号.add (输入流.nextInt());
        int[] 表 = new int [号号.size()];
        for (int 位 = 0; 位 < 号号.size(); ++位)
            表[位] = 号号.get (位);
        return 表;
    }


    private int process1 (final int[] 任务) {
        int 号 = 0;
        for (int 位 : 任务)
            号 += 位;
        return 号;
    }

    private int process2 (final int[] 任务) {
        /**
         * long, Long, LONG bit mask...
         * ... or set.
         */
        Set <Integer> 集合 = new HashSet <> (); // jíhé
        int 号 = 0;
        for (;;) {
            for (int 位 : 任务) {
                号 += 位;
                if (集合.contains (号))
                    return 号;
                else
                    集合.add (号);
            }
        }
    }

}
