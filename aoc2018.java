// -*- coding: utf-8; indent-tabs-mode: nil; mode: java; word-wrap: t -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2018-12-11 15:03:46 roukoru>

/*
 * http://adventofcode.com/2018

 * "We've detected some temporal anomalies," one of Santa's Elves at
 * the Temporal Anomaly Research and Detection Instrument Station
 * tells you. She sounded pretty worried when she called you down
 * here. "At 500-year intervals into the past, someone has been
 * changing Santa's history!"

 * "The good news is that the changes won't propagate to our time
 * stream for another 25 days, and we have a device" - she attaches
 * something to your wrist - "that will let you fix the changes with
 * no such propagation delay. It's configured to send you 500 years
 * further into the past every few days; that was the best we could do
 * on such short notice."

 * "The bad news is that we are detecting roughly fifty anomalies
 * throughout time; the device will indicate fixed anomalies with
 * stars. The other bad news is that we only have one device and
 * you're the best person for the job! Good lu--" She taps a button on
 * the device and you suddenly feel like you're falling. To save
 * Christmas, you need to get all fifty stars by December 25th.

 * Collect stars by solving puzzles. Two puzzles will be made
 * available on each day in the advent calendar; the second puzzle is
 * unlocked when you complete the first. Each puzzle grants one
 * star. Good luck!
 */

// package aoc2018;

// import java.lang.reflect.Constructor;
// import java.lang.Class;

//import java.io.Console; // System.console ();
// import java.io.BufferedReader;
// import java.io.File;
// import java.io.InputStreamReader;
// import java.io.IOException;

// import java.lang.Double;
// import java.lang.Integer;
// import java.lang.reflect.Method;
// import java.lang.String;

// import java.text.DateFormat;

// import java.util.ArrayList;
// import java.util.Collection;
// import java.util.Date;
// import java.util.function.Function;
// import java.util.HashSet;
// import java.util.List;
// import java.util.Scanner; // Scanner (System.in);
// import java.util.Set;
// import java.util.regex.Pattern;
// import java.util.regex.Matcher;
// import java.util.stream.Collectors;
// import java.util.stream.Stream;
// import java.util.Vector;


public class aoc2018 {

    public static boolean __DEBUG__ = false;

    public static void main (String[] args) throws Exception {
        (new aoc2018()).selector (args);
    }

    private void selector (final String[] args) {
        try {
            int day = Integer.parseInt (args[0]);
            switch (day) {
            case 1: {(new aoc2018_01()).eval (args); break;}
            case 2: {(new aoc2018_02()).eval (args); break;}
            case 3: {(new aoc2018_03()).eval (args); break;}
            case 4: {(new aoc2018_04()).eval (args); break;}
            case 5: {(new aoc2018_05()).eval (args); break;}
            case 6: {(new aoc2018_06()).eval (args); break;}
            case 7: {(new aoc2018_07()).eval (args); break;}
            case 8: {(new aoc2018_08()).eval (args); break;}
            case 9: {(new aoc2018_09()).eval (args); break;}
            case 10: {(new aoc2018_10()).eval (args); break;}
            case 11: {(new aoc2018_11()).eval (args); break;}
            // case 12: {(new aoc2018_12()).eval (args); break;}
            // case 13: {(new aoc2018_13()).eval (args); break;}
            // case 14: {(new aoc2018_14()).eval (args); break;}
            // case 15: {(new aoc2018_15()).eval (args); break;}
            // case 16: {(new aoc2018_16()).eval (args); break;}
            // case 17: {(new aoc2018_17()).eval (args); break;}
            // case 18: {(new aoc2018_18()).eval (args); break;}
            // case 19: {(new aoc2018_19()).eval (args); break;}
            // case 20: {(new aoc2018_20()).eval (args); break;}
            // case 21: {(new aoc2018_21()).eval (args); break;}
            // case 22: {(new aoc2018_22()).eval (args); break;}
            // case 23: {(new aoc2018_23()).eval (args); break;}
            // case 24: {(new aoc2018_24()).eval (args); break;}
            // case 25: {(new aoc2018_25()).eval (args); break;}
            default: System.out.println ("java aoc2018 day_number someargs");
            }
        }

        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}

