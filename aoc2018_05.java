// -*- coding: utf-8; indent-tabs-mode: nil; mode: java; word-wrap: t -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2018-12-12 10:44:25 roukoru>

/*
  http://adventofcode.com/2018/day/5

--- Day 5: Alchemical Reduction ---

You've managed to sneak in to the prototype suit manufacturing lab. The Elves are making decent progress, but are still struggling with the suit's size reduction capabilities.

While the very latest in 1518 alchemical technology might have solved their problem eventually, you can do better. You scan the chemical composition of the suit's material and discover that it is formed by extremely long polymers (one of which is available as your puzzle input).

The polymer is formed by smaller units which, when triggered, react with each other such that two adjacent units of the same type and opposite polarity are destroyed. Units' types are represented by letters; units' polarity is represented by capitalization. For instance, r and R are units with the same type but opposite polarity, whereas r and s are entirely different types and do not react.

For example:

    In aA, a and A react, leaving nothing behind.
    In abBA, bB destroys itself, leaving aA. As above, this then destroys itself, leaving nothing.
    In abAB, no two adjacent units are of the same type, and so nothing happens.
    In aabAAB, even though aa and AA are of the same type, their polarities match, and so nothing happens.

Now, consider a larger example, dabAcCaCBAcCcaDA:

dabAcCaCBAcCcaDA  The first 'cC' is removed.
dabAaCBAcCcaDA    This creates 'Aa', which is removed.
dabCBAcCcaDA      Either 'cC' or 'Cc' are removed (the result is the same).
dabCBAcaDA        No further actions can be taken.

After all possible reactions, the resulting polymer contains 10 units.

How many units remain after fully reacting the polymer you scanned?

--- Part Two ---

Time to improve the polymer.

One of the unit types is causing problems; it's preventing the polymer from collapsing as much as it should. Your goal is to figure out which unit type is causing the most problems, remove all instances of it (regardless of polarity), fully react the remaining polymer, and measure its length.

For example, again using the polymer dabAcCaCBAcCcaDA from above:

    Removing all A/a units produces dbcCCBcCcD. Fully reacting this polymer produces dbCBcD, which has length 6.
    Removing all B/b units produces daAcCaCAcCcaDA. Fully reacting this polymer produces daCAcaDA, which has length 8.
    Removing all C/c units produces dabAaBAaDA. Fully reacting this polymer produces daDA, which has length 4.
    Removing all D/d units produces abAcCaCBAcCcaA. Fully reacting this polymer produces abCBAc, which has length 6.

In this example, removing all C/c units was best, producing the answer 4.

What is the length of the shortest polymer you can produce by removing all units of exactly one type and fully reacting the result?

*/

// package aoc2018;

// import java.io.BufferedReader;
// import java.io.File;
// import java.io.InputStreamReader;
import java.io.IOException;

// import java.lang.Double;
import java.lang.Integer;
// import java.lang.reflect.Method;
// import java.lang.String;
// import java.lang.StringBuilder;

import java.nio.file.FileSystems;

// import java.security.MessageDigest;

// import java.text.DateFormat;

// import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
// import java.util.Collection;
// import java.util.Date;
// import java.util.Deque;
// import java.util.HashMap;
// import java.util.HashSet;
// import java.util.List;
// import java.util.Map;
import java.util.Scanner;
// import java.util.Set;
// import java.util.Vector;
// import java.util.function.Function;
// import java.util.regex.Matcher;
// import java.util.regex.Pattern;
// import java.util.stream.Collectors;
// import java.util.stream.Stream;

import java.util.logging.Level;
import java.util.logging.Logger;

public class aoc2018_05 implements AoC {

    final boolean __USE_LOG = false;
    //final Level __LOG_LEVEL = Level.FINE;
    final Level __LOG_LEVEL = Level.INFO;
    Logger log;
    final String test_input = "dabAcCaCBAcCcaDA";
    final String data_input = "data";

    public void help() {
        System.out.println
            ("java aoc2018 05 input_source\n" +
             "where input_source may be:\n" +
             "test\t common test data from aoc (default, if source not specified)\n" +
             "builtin\t builtin use-specific data from aoc\n"+
             "-\t use stdin\n" +
             "file\t use file");
    }

    public void eval (final String[] args) throws Exception { // main ... a sort of
        log = Logger.getGlobal();
        log.setLevel (__LOG_LEVEL);
        try {
            String 任务 = read_parse_input (args); // rènwù
            System.out.println (process1 (任务));
            System.out.println (process2 (任务));
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private Scanner get_input_scanner (final String 源) throws IOException {
        /**
         * helper for getting correct instance of Scanner for
         * reading lines from:
         * stdin (源="-")
         * or other file
         */
         return 源.equals("-") ?
            new Scanner (System.in) :
            new Scanner (FileSystems.getDefault().getPath (源));
    }

    private String read_lines (final String 源) throws IOException {
        /**
         * read lines from stdin (源="-") or other file
         */
        Scanner 输入流 = get_input_scanner (源); // shūrù liú
        for (String 串; 输入流.hasNext();) {
            串 = 输入流.nextLine();
            if (串 != null && 串.length() > 0)
                return 串;
        }
        return null;
    }

    private String read_parse_input (final String[] args)
        throws IOException {
        String 串川 = null;
        if (args.length < 2)
            串川 = test_input;
        else {
            if (args[1].equals ("test"))
                串川 = test_input;
            else
                if (args[1].equals ("builtin")) {
                    System.out.println ("no builtin data");
                    System.exit (1);
                }
                else
                    串川 = args[1].equals ("-") ? read_lines ("-") : args[1];
        }
        return 串川;
    }

    final static byte MASK_CAPS = 0x20,
        MASK_CAPS_NEG = ~0x20;
    private int process1 (final String 任务) {
        //System.out.println (任务);
        byte[] 串 = 任务.getBytes();
        byte[] 链; // liàn
        byte 甲;
        int 长 = 0, 位;
        boolean 荐; // jiàn
        for (荐 = true; 荐;) {
            荐 = false;
            链 = new byte [串.length];
            for (位 = 0, 长 = 0; 位 < (串.length - 1); ++位) {
                if((串[位] & MASK_CAPS_NEG) != (串[位+1] & MASK_CAPS_NEG)) {
                        链[长++] = 串[位];
                        continue;
                }
                if ((串[位] ^ MASK_CAPS) == 串[位+1]) {
                    ++位;
                    荐 = true;
                }
                else
                    链[长++] = 串[位];
            }
            if (位 < 串.length)
                链[长++] = 串[位];
            串 = Arrays.copyOf (链, 长);
        }
        //System.out.println (new String (串));
        //System.out.println (Arrays.toString (串));
        return 串.length;
    }

    private String process2 (final String 任务) {
        return "done!";
    }

}
