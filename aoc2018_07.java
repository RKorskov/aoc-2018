// -*- coding: utf-8; indent-tabs-mode: nil; mode: java; word-wrap: t -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2018-12-11 11:03:32 roukoru>

/*
  http://adventofcode.com/2018/day/7

--- Day 7: The Sum of Its Parts ---

You find yourself standing on a snow-covered coastline; apparently, you landed a little off course. The region is too hilly to see the North Pole from here, but you do spot some Elves that seem to be trying to unpack something that washed ashore. It's quite cold out, so you decide to risk creating a paradox by asking them for directions.

"Oh, are you the search party?" Somehow, you can understand whatever Elves from the year 1018 speak; you assume it's Ancient Nordic Elvish. Could the device on your wrist also be a translator? "Those clothes don't look very warm; take this." They hand you a heavy coat.

"We do need to find our way back to the North Pole, but we have higher priorities at the moment. You see, believe it or not, this box contains something that will solve all of Santa's transportation problems - at least, that's what it looks like from the pictures in the instructions." It doesn't seem like they can read whatever language it's in, but you can: "Sleigh kit. Some assembly required."

"'Sleigh'? What a wonderful name! You must help us assemble this 'sleigh' at once!" They start excitedly pulling more parts out of the box.

The instructions specify a series of steps and requirements about which steps must be finished before others can begin (your puzzle input). Each step is designated by a single letter. For example, suppose you have the following instructions:

Step C must be finished before step A can begin.
Step C must be finished before step F can begin.
Step A must be finished before step B can begin.
Step A must be finished before step D can begin.
Step B must be finished before step E can begin.
Step D must be finished before step E can begin.
Step F must be finished before step E can begin.

Visually, these requirements look like this:


  -->A--->B--
 /    \      \
C      -->D----->E
 \           /
  ---->F-----

Your first goal is to determine the order in which the steps should be completed. If more than one step is ready, choose the step which is first alphabetically. In this example, the steps would be completed as follows:

    Only C is available, and so it is done first.
    Next, both A and F are available. A is first alphabetically, so it is done next.
    Then, even though F was available earlier, steps B and D are now also available, and B is the first alphabetically of the three.
    After that, only D and F are available. E is not available because only some of its prerequisites are complete. Therefore, D is completed next.
    F is the only choice, so it is done next.
    Finally, E is completed.

So, in this example, the correct order is CABDFE.

In what order should the steps in your instructions be completed?

*/

// package aoc2018;

// import java.io.BufferedReader;
// import java.io.File;
// import java.io.InputStreamReader;
import java.io.IOException;

// import java.lang.Double;
import java.lang.Integer;
// import java.lang.reflect.Method;
// import java.lang.String;
// import java.lang.StringBuilder;

import java.nio.file.FileSystems;

// import java.security.MessageDigest;

// import java.text.DateFormat;

// import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
// import java.util.Collection;
// import java.util.Date;
// import java.util.Deque;
// import java.util.HashMap;
// import java.util.HashSet;
// import java.util.List;
// import java.util.Map;
import java.util.Scanner;
// import java.util.Set;
// import java.util.Vector;
// import java.util.function.Function;
// import java.util.regex.Matcher;
// import java.util.regex.Pattern;
// import java.util.stream.Collectors;
// import java.util.stream.Stream;

import java.util.logging.Level;
import java.util.logging.Logger;

public class aoc2018_07 implements AoC {

    final boolean __USE_LOG = false;
    //final Level __LOG_LEVEL = Level.FINE;
    final Level __LOG_LEVEL = Level.INFO;
    Logger log;
    final String[] test_input = {"test"};
    final String[] data_input = {"data"};

    public void help() {
        System.out.println
            ("java aoc2018 07 input_source\n" +
             "where input_source may be:\n" +
             "test\t common test data from aoc (default, if source not specified)\n" +
             "builtin\t builtin use-specific data from aoc\n"+
             "-\t use stdin\n" +
             "file\t use file");
    }

    public void eval (final String[] args) throws Exception { // main ... a sort of
        log = Logger.getGlobal();
        log.setLevel (__LOG_LEVEL);
        // if (args.length < 1) return;
        // int[][] 任务 = read_input (args[1]);
        try {
            InputData 任务 = read_parse_input (args); // rènwù
            System.out.println (process1 (任务));
            System.out.println (process2 (任务));
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private Scanner get_input_scanner (final String 源) throws IOException {
        /**
         * helper for getting correct instance of Scanner for
         * reading lines from:
         * stdin (源="-")
         * or other file
         */
         return 源.equals("-") ?
            new Scanner (System.in) :
            new Scanner (FileSystems.getDefault().getPath (源));
    }

    private String[] read_lines (final String 源) throws IOException {
        /**
         * read lines from stdin (源="-") or other file
         */
        Scanner 输入流 = get_input_scanner (源); // shūrù liú
        ArrayList <String> 矩阵 = new ArrayList <String> (); // jǔzhèn
        for (String 串; 输入流.hasNext();) {
            串 = 输入流.nextLine();
            if (串 != null && 串.length() > 0)
                矩阵.add (串);
        }
        return 矩阵.toArray (new String [矩阵.size()]);
    }

    private int[][] read_input (final String 源) throws IOException {
        /**
         * read lines from 源 and converts them to int triplets
         * 1x1x10
         * 2x3x4
         */
        Scanner 输入流 = get_input_scanner (源);
        ArrayList <int[]> 号号 = new ArrayList <> ();
        for (String 串; 输入流.hasNext();) {
            串 = 输入流.nextLine();
            if (__USE_LOG) {
                Logger 木头 = Logger.getGlobal();
                木头.info (串);
            }
            if (串 == null || 串.length() < 5) break;
            String[] 字 = 串.split ("x");
            int[] 号 = new int[3];
            for (int 甲 = 0; 甲 < 号.length; ++甲) // && 甲 < 字.length
                号[甲] = Integer.parseInt (字[甲]);
            号号.add (号);
        }
        return 号号.toArray (new int[号号.size()][]);
    }

    private InputData read_parse_input (final String[] args)
        throws IOException {
        String[] 串川;
        if (args.length < 2)
            串川 = test_input;
        else {
            if (args[1].equals ("test"))
                串川 = test_input;
            else
                if (args[1].equals ("builtin"))
                    串川 = data_input;
                else
                    串川 = read_lines (args[1].equals ("-") ? "-" : args[1]);
        }
        return parse_input (串川);
    }

    private InputData parse_input (final String[] 任务) {
        InputData 桂马 = new InputData (任务); // gui ma
        return 桂马;
    }

    private String process1 (final InputData 任务) {
        return "done!";
    }

    private String process2 (final InputData 任务) {
        return "done!";
    }


    class InputData {
        String[] 表;
        InputData() {表 = null;}
        InputData (final String[] 目录) {表 = Arrays.copyOf (目录, 目录.length);}
        public String toString() {return Arrays.toString (表);}
    }

}
