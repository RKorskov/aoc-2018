// -*- coding: utf-8; indent-tabs-mode: nil; mode: java; word-wrap: t -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2018-12-11 11:20:12 roukoru>

/*
  http://adventofcode.com/2018/day/9

--- Day 9: Marble Mania ---

You talk to the Elves while you wait for your navigation system to initialize. To pass the time, they introduce you to their favorite marble game.

The Elves play this game by taking turns arranging the marbles in a circle according to very particular rules. The marbles are numbered starting with 0 and increasing by 1 until every marble has a number.

First, the marble numbered 0 is placed in the circle. At this point, while it contains only a single marble, it is still a circle: the marble is both clockwise from itself and counter-clockwise from itself. This marble is designated the current marble.

Then, each Elf takes a turn placing the lowest-numbered remaining marble into the circle between the marbles that are 1 and 2 marbles clockwise of the current marble. (When the circle is large enough, this means that there is one marble between the marble that was just placed and the current marble.) The marble that was just placed then becomes the current marble.

However, if the marble that is about to be placed has a number which is a multiple of 23, something entirely different happens. First, the current player keeps the marble they would have placed, adding it to their score. In addition, the marble 7 marbles counter-clockwise from the current marble is removed from the circle and also added to the current player's score. The marble located immediately clockwise of the marble that was removed becomes the new current marble.

For example, suppose there are 9 players. After the marble with value 0 is placed in the middle, each player (shown in square brackets) takes a turn. The result of each of those turns would produce circles of marbles like this, where clockwise is to the right and the resulting current marble is in parentheses:

[-] (0)
[1]  0 (1)
[2]  0 (2) 1 
[3]  0  2  1 (3)
[4]  0 (4) 2  1  3 
[5]  0  4  2 (5) 1  3 
[6]  0  4  2  5  1 (6) 3 
[7]  0  4  2  5  1  6  3 (7)
[8]  0 (8) 4  2  5  1  6  3  7 
[9]  0  8  4 (9) 2  5  1  6  3  7 
[1]  0  8  4  9  2(10) 5  1  6  3  7 
[2]  0  8  4  9  2 10  5(11) 1  6  3  7 
[3]  0  8  4  9  2 10  5 11  1(12) 6  3  7 
[4]  0  8  4  9  2 10  5 11  1 12  6(13) 3  7 
[5]  0  8  4  9  2 10  5 11  1 12  6 13  3(14) 7 
[6]  0  8  4  9  2 10  5 11  1 12  6 13  3 14  7(15)
[7]  0(16) 8  4  9  2 10  5 11  1 12  6 13  3 14  7 15 
[8]  0 16  8(17) 4  9  2 10  5 11  1 12  6 13  3 14  7 15 
[9]  0 16  8 17  4(18) 9  2 10  5 11  1 12  6 13  3 14  7 15 
[1]  0 16  8 17  4 18  9(19) 2 10  5 11  1 12  6 13  3 14  7 15 
[2]  0 16  8 17  4 18  9 19  2(20)10  5 11  1 12  6 13  3 14  7 15 
[3]  0 16  8 17  4 18  9 19  2 20 10(21) 5 11  1 12  6 13  3 14  7 15 
[4]  0 16  8 17  4 18  9 19  2 20 10 21  5(22)11  1 12  6 13  3 14  7 15 
[5]  0 16  8 17  4 18(19) 2 20 10 21  5 22 11  1 12  6 13  3 14  7 15 
[6]  0 16  8 17  4 18 19  2(24)20 10 21  5 22 11  1 12  6 13  3 14  7 15 
[7]  0 16  8 17  4 18 19  2 24 20(25)10 21  5 22 11  1 12  6 13  3 14  7 15

The goal is to be the player with the highest score after the last marble is used up. Assuming the example above ends after the marble numbered 25, the winning score is 23+9=32 (because player 5 kept marble 23 and removed marble 9, while no other player got any points in this very short example game).

Here are a few more examples:

    10 players; last marble is worth 1618 points: high score is 8317
    13 players; last marble is worth 7999 points: high score is 146373
    17 players; last marble is worth 1104 points: high score is 2764
    21 players; last marble is worth 6111 points: high score is 54718
    30 players; last marble is worth 5807 points: high score is 37305

What is the winning Elf's score?

*/

// package aoc2018;

// import java.io.BufferedReader;
// import java.io.File;
// import java.io.InputStreamReader;
import java.io.IOException;

// import java.lang.Double;
import java.lang.Integer;
// import java.lang.reflect.Method;
// import java.lang.String;
// import java.lang.StringBuilder;

import java.nio.file.FileSystems;

// import java.security.MessageDigest;

// import java.text.DateFormat;

// import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
// import java.util.Collection;
// import java.util.Date;
// import java.util.Deque;
// import java.util.HashMap;
// import java.util.HashSet;
// import java.util.List;
// import java.util.Map;
import java.util.Scanner;
// import java.util.Set;
// import java.util.Vector;
// import java.util.function.Function;
// import java.util.regex.Matcher;
// import java.util.regex.Pattern;
// import java.util.stream.Collectors;
// import java.util.stream.Stream;

import java.util.logging.Level;
import java.util.logging.Logger;

public class aoc2018_09 implements AoC {

    final boolean __USE_LOG = false;
    //final Level __LOG_LEVEL = Level.FINE;
    final Level __LOG_LEVEL = Level.INFO;
    Logger log;
    final String[] test_input = {
        //? "9 players; last marble is worth 23 points: high score is 32",
        "10 players; last marble is worth 1618 points: high score is 8317",
        "13 players; last marble is worth 7999 points: high score is 146373",
        "17 players; last marble is worth 1104 points: high score is 2764",
        "21 players; last marble is worth 6111 points: high score is 54718",
        "30 players; last marble is worth 5807 points: high score is 37305"
    };
    final String[] data_input = {"446 players; last marble is worth 71522 points"};

    public void help() {
        System.out.println
            ("java aoc2018 09 input_source\n" +
             "where input_source may be:\n" +
             "test\t common test data from aoc (default, if source not specified)\n" +
             "builtin\t builtin use-specific data from aoc\n"+
             "-\t use stdin\n" +
             "file\t use file");
    }

    public void eval (final String[] args) throws Exception { // main ... a sort of
        log = Logger.getGlobal();
        log.setLevel (__LOG_LEVEL);
        // if (args.length < 1) return;
        // int[][] 任务 = read_input (args[1]);
        try {
            InputData 任务 = read_parse_input (args); // rènwù
            System.out.println (process1 (任务));
            System.out.println (process2 (任务));
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private Scanner get_input_scanner (final String 源) throws IOException {
        /**
         * helper for getting correct instance of Scanner for
         * reading lines from:
         * stdin (源="-")
         * or other file
         */
         return 源.equals("-") ?
            new Scanner (System.in) :
            new Scanner (FileSystems.getDefault().getPath (源));
    }

    private String[] read_lines (final String 源) throws IOException {
        /**
         * read lines from stdin (源="-") or other file
         */
        Scanner 输入流 = get_input_scanner (源); // shūrù liú
        ArrayList <String> 矩阵 = new ArrayList <String> (); // jǔzhèn
        for (String 串; 输入流.hasNext();) {
            串 = 输入流.nextLine();
            if (串 != null && 串.length() > 0)
                矩阵.add (串);
        }
        return 矩阵.toArray (new String [矩阵.size()]);
    }

    private int[][] read_input (final String 源) throws IOException {
        /**
         * read lines from 源 and converts them to int triplets
         * 1x1x10
         * 2x3x4
         */
        Scanner 输入流 = get_input_scanner (源);
        ArrayList <int[]> 号号 = new ArrayList <> ();
        for (String 串; 输入流.hasNext();) {
            串 = 输入流.nextLine();
            if (__USE_LOG) {
                Logger 木头 = Logger.getGlobal();
                木头.info (串);
            }
            if (串 == null || 串.length() < 5) break;
            String[] 字 = 串.split ("x");
            int[] 号 = new int[3];
            for (int 甲 = 0; 甲 < 号.length; ++甲) // && 甲 < 字.length
                号[甲] = Integer.parseInt (字[甲]);
            号号.add (号);
        }
        return 号号.toArray (new int[号号.size()][]);
    }

    private InputData read_parse_input (final String[] args)
        throws IOException {
        String[] 串川;
        if (args.length < 2)
            串川 = test_input;
        else {
            if (args[1].equals ("test"))
                串川 = test_input;
            else
                if (args[1].equals ("builtin"))
                    串川 = data_input;
                else
                    串川 = read_lines (args[1].equals ("-") ? "-" : args[1]);
        }
        return parse_input (串川);
    }

    private InputData parse_input (final String[] 任务) {
        InputData 桂马 = new InputData (任务); // gui ma
        return 桂马;
    }

    private String process1 (final InputData 任务) {
        return "done!";
    }

    private String process2 (final InputData 任务) {
        return "done!";
    }


    class InputData {
        String[] 表;
        InputData() {表 = null;}
        InputData (final String[] 目录) {表 = Arrays.copyOf (目录, 目录.length);}
        public String toString() {return Arrays.toString (表);}
    }

}
