// -*- coding: utf-8; indent-tabs-mode: nil; mode: java; word-wrap: t -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2018-12-08 23:02:50 roukoru>

/*
  http://adventofcode.com/2018/day/4

--- Day 4: Repose Record ---

You've sneaked into another supply closet - this time, it's across from the prototype suit manufacturing lab. You need to sneak inside and fix the issues with the suit, but there's a guard stationed outside the lab, so this is as close as you can safely get.

As you search the closet for anything that might help, you discover that you're not the first person to want to sneak in. Covering the walls, someone has spent an hour starting every midnight for the past few months secretly observing this guard post! They've been writing down the ID of the one guard on duty that night - the Elves seem to have decided that one guard was enough for the overnight shift - as well as when they fall asleep or wake up while at their post (your puzzle input).

For example, consider the following records, which have already been organized into chronological order:

[1518-11-01 00:00] Guard #10 begins shift
[1518-11-01 00:05] falls asleep
[1518-11-01 00:25] wakes up
[1518-11-01 00:30] falls asleep
[1518-11-01 00:55] wakes up
[1518-11-01 23:58] Guard #99 begins shift
[1518-11-02 00:40] falls asleep
[1518-11-02 00:50] wakes up
[1518-11-03 00:05] Guard #10 begins shift
[1518-11-03 00:24] falls asleep
[1518-11-03 00:29] wakes up
[1518-11-04 00:02] Guard #99 begins shift
[1518-11-04 00:36] falls asleep
[1518-11-04 00:46] wakes up
[1518-11-05 00:03] Guard #99 begins shift
[1518-11-05 00:45] falls asleep
[1518-11-05 00:55] wakes up

Timestamps are written using year-month-day hour:minute format. The guard falling asleep or waking up is always the one whose shift most recently started. Because all asleep/awake times are during the midnight hour (00:00 - 00:59), only the minute portion (00 - 59) is relevant for those events.

Visually, these records show that the guards are asleep at these times:

Date   ID   Minute
            000000000011111111112222222222333333333344444444445555555555
            012345678901234567890123456789012345678901234567890123456789
11-01  #10  .....####################.....#########################.....
11-02  #99  ........................................##########..........
11-03  #10  ........................#####...............................
11-04  #99  ....................................##########..............
11-05  #99  .............................................##########.....

The columns are Date, which shows the month-day portion of the relevant day; ID, which shows the guard on duty that day; and Minute, which shows the minutes during which the guard was asleep within the midnight hour. (The Minute column's header shows the minute's ten's digit in the first row and the one's digit in the second row.) Awake is shown as ., and asleep is shown as #.

Note that guards count as asleep on the minute they fall asleep, and they count as awake on the minute they wake up. For example, because Guard #10 wakes up at 00:25 on 1518-11-01, minute 25 is marked as awake.

If you can figure out the guard most likely to be asleep at a specific time, you might be able to trick that guard into working tonight so you can have the best chance of sneaking in. You have two strategies for choosing the best guard/minute combination.

Strategy 1: Find the guard that has the most minutes asleep. What minute does that guard spend asleep the most?

In the example above, Guard #10 spent the most minutes asleep, a total of 50 minutes (20+25+5), while Guard #99 only slept for a total of 30 minutes (10+10+10). Guard #10 was asleep most during minute 24 (on two days, whereas any other minute the guard was asleep was only seen on one day).

While this example listed the entries in chronological order, your entries are in the order you found them. You'll need to organize them before they can be analyzed.

What is the ID of the guard you chose multiplied by the minute you chose? (In the above example, the answer would be 10 * 24 = 240.)
*/

// package aoc2018;

// import java.io.BufferedReader;
// import java.io.File;
// import java.io.InputStreamReader;
import java.io.IOException;

// import java.lang.Double;
import java.lang.Integer;
// import java.lang.reflect.Method;
// import java.lang.StringBuilder;

import java.nio.file.FileSystems;

// import java.security.MessageDigest;

// import java.text.DateFormat;

// import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
// import java.util.Collection;
// import java.util.Date;
// import java.util.Deque;
import java.util.HashMap;
// import java.util.HashSet;
// import java.util.List;
import java.util.Map;
import java.util.Scanner;
// import java.util.Set;
// import java.util.Vector;
// import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
// import java.util.stream.Collectors;
// import java.util.stream.Stream;

import java.util.logging.Level;
import java.util.logging.Logger;

public class aoc2018_04 implements AoC {

    final boolean __USE_LOG = false;
    //final Level __LOG_LEVEL = Level.FINE;
    final Level __LOG_LEVEL = Level.INFO;
    Logger log;
    final String[] test_input = {
        "[1518-11-01 00:00] Guard #10 begins shift",
        "[1518-11-01 00:05] falls asleep",
        "[1518-11-01 00:25] wakes up",
        "[1518-11-01 00:30] falls asleep",
        "[1518-11-01 00:55] wakes up",
        "[1518-11-01 23:58] Guard #99 begins shift",
        "[1518-11-02 00:40] falls asleep",
        "[1518-11-02 00:50] wakes up",
        "[1518-11-03 00:05] Guard #10 begins shift",
        "[1518-11-03 00:24] falls asleep",
        "[1518-11-03 00:29] wakes up",
        "[1518-11-04 00:02] Guard #99 begins shift",
        "[1518-11-04 00:36] falls asleep",
        "[1518-11-04 00:46] wakes up",
        "[1518-11-05 00:03] Guard #99 begins shift",
        "[1518-11-05 00:45] falls asleep",
        "[1518-11-05 00:55] wakes up"
    };
    final String[] data_input = {"data"};

    public void help() {
        System.out.println
            ("java aoc2018 04 input_source\n" +
             "where input_source may be:\n" +
             "test\t common test data from aoc (default, if source not specified)\n" +
             "builtin\t builtin use-specific data from aoc\n"+
             "-\t use stdin\n" +
             "file\t use file");
    }

    public void eval (final String[] args) throws Exception { // main ... a sort of
        log = Logger.getGlobal();
        log.setLevel (__LOG_LEVEL);
        // if (args.length < 1) return;
        // int[][] 任务 = read_input (args[1]);
        try {
            InputData 任务 = read_parse_input (args); // rènwù
            System.out.println (process1 (任务));
            System.out.println (process2 (任务));
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private Scanner get_input_scanner (final String src) throws IOException {
        /**
         * helper for getting correct instance of Scanner for
         * reading lines from:
         * stdin (src="-")
         * or other file
         */
         return src.equals("-") ?
            new Scanner (System.in) :
            new Scanner (FileSystems.getDefault().getPath (src));
    }

    private String[] read_lines (final String src) throws IOException {
        /**
         * read lines from stdin (src="-") or other file
         */
        Scanner 输入流 = get_input_scanner (src); // shūrù liú
        ArrayList <String> 矩阵 = new ArrayList <String> (); // jǔzhèn
        for (String 串; 输入流.hasNext();) {
            串 = 输入流.nextLine();
            // [1518-03-04 00:42] wakes up
            if (串 != null && 串.length() > 26)
                矩阵.add (串);
        }
        String[] 表 = 矩阵.toArray (new String [矩阵.size()]);
        Arrays.parallelSort (表);
        return 表;
    }

    private InputData read_parse_input (final String[] args)
        throws IOException {
        String[] 串川;
        if (args.length < 2)
            串川 = test_input;
        else {
            if (args[1].equals ("test"))
                串川 = test_input;
            else
                if (args[1].equals ("builtin"))
                    串川 = data_input;
                else
                    串川 = read_lines (args[1].equals ("-") ? "-" : args[1]);
        }
        return parse_input (串川);
    }

    private InputData parse_input (final String[] 任务) {
        InputData 桂马 = new InputData (任务); // gui ma
        Map <Integer,int[]> 卫兵 = build_watch_list(任务);
        えいへい[] 表 = new えいへい[卫兵.size()];
        int 位 = 0;
        for (Integer 号 : 卫兵.keySet()) {
            log.info (String.format ("%d %s", 号, Arrays.toString (卫兵.get (号))));
            表[位++] = new えいへい (号, 卫兵.get (号));
        }
        桂马.卫兵 = 表;
        return 桂马;
    }

    // [1518-11-05 00:03] Guard #99 begins shift
    final Pattern 路首 = Pattern.compile ("^\\[1518-\\d{2}-\\d{2} \\d{2}:\\d{2}\\] Guard #(?<gid>\\d+) begins shift$"), // lù shǒu
        路觉 = Pattern.compile ("^\\[1518-\\d{2}-\\d{2} \\d{2}:(?<min>\\d+)\\] (?<act>(falls asleep)|(wakes up))$"); // lù jiào
    // final String ACT_SLEEP = "falls asleep", ACT_AWAKE = "wakes up";

    private Map <Integer,int[]> build_watch_list (final String[] 任务) {
        Map <Integer,int[]> 卫兵 = new HashMap <> ();
        ArrayList <Integer> 从钟 = new ArrayList <> (); // cóng zhōng
        int[] 向量; // xiàng liàng
        int 卫兵号 = -1, 位, 行; // háng
        Matcher 配;
        for (行 = 0; 行 < 任务.length; ++行) {
            配 = 路首.matcher (任务[行]);
            if (配 != null && 配.matches()) {
                if (卫兵号 > 0 && 从钟.size() > 0)
                    卫兵.put (卫兵号, move2array (从钟));
                卫兵号 = Integer.parseInt (配.group ("gid"));
                continue;
            }
            配 = 路觉.matcher (任务[行]);
            if (配 != null && 配.matches())
                从钟.add (Integer.parseInt (配.group ("min")));
        }
        if (卫兵号 > 0 && 从钟.size() > 0)
            卫兵.put (卫兵号, move2array(从钟));
        return 卫兵;
    }

    private int[] move2array (ArrayList <Integer> 从钟) {
        int[] 向量 = new int [从钟.size()]; // xiàng liàng
        int 位 = 0;
        for (int 号 : 从钟)
            向量[位++] = 号;
        从钟.clear();
        return 向量;
    }

    private String process1 (final InputData 任务) {
        System.out.println (任务.卫兵.length);
        for (えいへい 兵 : 任务.卫兵)
            System.out.printf ("%d %s\n", 兵.第号, Arrays.toString (兵.从钟));
        return "done!";
    }

    private String process2 (final InputData 任务) {
        return "done!";
    }


    class InputData {
        えいへい[] 卫兵;
        // Map <Integer,int[]> 卫兵;
        String[] 表;
        InputData() {表 = null; 卫兵 = null;}
        InputData (final String[] 目录) {
            卫兵 = null;
            表 = Arrays.copyOf (目录, 目录.length);
        }
        public String toString() {return Arrays.toString (表);}
    }

    class えいへい {
        /**
         * [1518-03-02 23:56] Guard #2441 begins shift
         * [1518-03-03 00:48] falls asleep
         * [1518-03-03 00:50] wakes up
         * [1518-03-03 00:53] falls asleep
         * [1518-03-03 00:57] wakes up
         *
         * 2441: {48 50 53 57}
         */
        int 第号;
        int[] 从钟;

        えいへい() {
            第号 = -1;
            从钟 = null;
        }

        えいへい (final int 号, final int[] 分钟) {
            第号 = 号;
            从钟 = 分钟;
        }
    }
}
