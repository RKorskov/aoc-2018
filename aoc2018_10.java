// -*- coding: utf-8; indent-tabs-mode: nil; mode: java; word-wrap: t -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2018-12-12 01:14:50 roukoru>

/*
  http://adventofcode.com/2018/day/10

--- Day 10: The Stars Align ---

It's no use; your navigation system simply isn't capable of providing walking directions in the arctic circle, and certainly not in 1018.

The Elves suggest an alternative. In times like these, North Pole rescue operations will arrange points of light in the sky to guide missing Elves back to base. Unfortunately, the message is easy to miss: the points move slowly enough that it takes hours to align them, but have so much momentum that they only stay aligned for a second. If you blink at the wrong time, it might be hours before another message appears.

You can see these points of light floating in the distance, and record their position in the sky and their velocity, the relative change in position per second (your puzzle input). The coordinates are all given from your perspective; given enough time, those positions and velocities will move the points into a cohesive message!

Rather than wait, you decide to fast-forward the process and calculate what the points will eventually spell.

For example, suppose you note the following points:

position=< 9,  1> velocity=< 0,  2>
position=< 7,  0> velocity=<-1,  0>
position=< 3, -2> velocity=<-1,  1>
position=< 6, 10> velocity=<-2, -1>
position=< 2, -4> velocity=< 2,  2>
position=<-6, 10> velocity=< 2, -2>
position=< 1,  8> velocity=< 1, -1>
position=< 1,  7> velocity=< 1,  0>
position=<-3, 11> velocity=< 1, -2>
position=< 7,  6> velocity=<-1, -1>
position=<-2,  3> velocity=< 1,  0>
position=<-4,  3> velocity=< 2,  0>
position=<10, -3> velocity=<-1,  1>
position=< 5, 11> velocity=< 1, -2>
position=< 4,  7> velocity=< 0, -1>
position=< 8, -2> velocity=< 0,  1>
position=<15,  0> velocity=<-2,  0>
position=< 1,  6> velocity=< 1,  0>
position=< 8,  9> velocity=< 0, -1>
position=< 3,  3> velocity=<-1,  1>
position=< 0,  5> velocity=< 0, -1>
position=<-2,  2> velocity=< 2,  0>
position=< 5, -2> velocity=< 1,  2>
position=< 1,  4> velocity=< 2,  1>
position=<-2,  7> velocity=< 2, -2>
position=< 3,  6> velocity=<-1, -1>
position=< 5,  0> velocity=< 1,  0>
position=<-6,  0> velocity=< 2,  0>
position=< 5,  9> velocity=< 1, -2>
position=<14,  7> velocity=<-2,  0>
position=<-3,  6> velocity=< 2, -1>

Each line represents one point. Positions are given as <X, Y> pairs: X represents how far left (negative) or right (positive) the point appears, while Y represents how far up (negative) or down (positive) the point appears.

At 0 seconds, each point has the position given. Each second, each point's velocity is added to its position. So, a point with velocity <1, -2> is moving to the right, but is moving upward twice as quickly. If this point's initial position were <3, 9>, after 3 seconds, its position would become <6, 3>.

Over time, the points listed above would move like this:

Initially:
........#.............
................#.....
.........#.#..#.......
......................
#..........#.#.......#
...............#......
....#.................
..#.#....#............
.......#..............
......#...............
...#...#.#...#........
....#..#..#.........#.
.......#..............
...........#..#.......
#...........#.........
...#.......#..........

After 1 second:
......................
......................
..........#....#......
........#.....#.......
..#.........#......#..
......................
......#...............
....##.........#......
......#.#.............
.....##.##..#.........
........#.#...........
........#...#.....#...
..#...........#.......
....#.....#.#.........
......................
......................

After 2 seconds:
......................
......................
......................
..............#.......
....#..#...####..#....
......................
........#....#........
......#.#.............
.......#...#..........
.......#..#..#.#......
....#....#.#..........
.....#...#...##.#.....
........#.............
......................
......................
......................

After 3 seconds:
......................
......................
......................
......................
......#...#..###......
......#...#...#.......
......#...#...#.......
......#####...#.......
......#...#...#.......
......#...#...#.......
......#...#...#.......
......#...#..###......
......................
......................
......................
......................

After 4 seconds:
......................
......................
......................
............#.........
........##...#.#......
......#.....#..#......
.....#..##.##.#.......
.......##.#....#......
...........#....#.....
..............#.......
....#......#...#......
.....#.....##.........
...............#......
...............#......
......................
......................

After 3 seconds, the message appeared briefly: HI. Of course, your message will be much longer and will take many more seconds to appear.

What message will eventually appear in the sky?

XLZAKBGZ

--- Part Two ---

Good thing you didn't have to wait, because that would have taken a long time - much longer than the 3 seconds in the example above.

Impressed by your sub-hour communication capabilities, the Elves are curious: exactly how many seconds would they have needed to wait for that message to appear?

10656

*/

// package aoc2018;

// import java.io.BufferedReader;
// import java.io.File;
// import java.io.InputStreamReader;
import java.io.IOException;

// import java.lang.Double;
import java.lang.Integer;
// import java.lang.reflect.Method;
// import java.lang.String;
import java.lang.StringBuilder;
import java.lang.IllegalArgumentException;
import java.lang.Math;

import java.nio.file.FileSystems;

// import java.security.MessageDigest;

// import java.text.DateFormat;

// import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
// import java.util.Collection;
// import java.util.Date;
// import java.util.Deque;
// import java.util.HashMap;
// import java.util.HashSet;
// import java.util.List;
// import java.util.Map;
import java.util.Scanner;
// import java.util.Set;
// import java.util.Vector;
// import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
// import java.util.stream.Collectors;
// import java.util.stream.Stream;

import java.util.logging.Level;
import java.util.logging.Logger;

public class aoc2018_10 implements AoC {

    final static Object NULL = null;
    final static boolean t = true;
    final static boolean nil = false;

    final boolean __USE_LOG = false;
    //final Level __LOG_LEVEL = Level.FINE;
    final Level __LOG_LEVEL = Level.INFO;
    Logger log;
    final String[] test_input = {
        "position=< 9,  1> velocity=< 0,  2>",
        "position=< 7,  0> velocity=<-1,  0>",
        "position=< 3, -2> velocity=<-1,  1>",
        "position=< 6, 10> velocity=<-2, -1>",
        "position=< 2, -4> velocity=< 2,  2>",
        "position=<-6, 10> velocity=< 2, -2>",
        "position=< 1,  8> velocity=< 1, -1>",
        "position=< 1,  7> velocity=< 1,  0>",
        "position=<-3, 11> velocity=< 1, -2>",
        "position=< 7,  6> velocity=<-1, -1>",
        "position=<-2,  3> velocity=< 1,  0>",
        "position=<-4,  3> velocity=< 2,  0>",
        "position=<10, -3> velocity=<-1,  1>",
        "position=< 5, 11> velocity=< 1, -2>",
        "position=< 4,  7> velocity=< 0, -1>",
        "position=< 8, -2> velocity=< 0,  1>",
        "position=<15,  0> velocity=<-2,  0>",
        "position=< 1,  6> velocity=< 1,  0>",
        "position=< 8,  9> velocity=< 0, -1>",
        "position=< 3,  3> velocity=<-1,  1>",
        "position=< 0,  5> velocity=< 0, -1>",
        "position=<-2,  2> velocity=< 2,  0>",
        "position=< 5, -2> velocity=< 1,  2>",
        "position=< 1,  4> velocity=< 2,  1>",
        "position=<-2,  7> velocity=< 2, -2>",
        "position=< 3,  6> velocity=<-1, -1>",
        "position=< 5,  0> velocity=< 1,  0>",
        "position=<-6,  0> velocity=< 2,  0>",
        "position=< 5,  9> velocity=< 1, -2>",
        "position=<14,  7> velocity=<-2,  0>",
        "position=<-3,  6> velocity=< 2, -1>"
    };
    final String[] data_input = {"assignment data"};

    public void help() {
        System.out.println
            ("java aoc2018 10 input_source\n" +
             "where input_source may be:\n" +
             "test\t common test data from aoc (default, if source not specified)\n" +
             "builtin\t builtin use-specific data from aoc\n"+
             "-\t use stdin\n" +
             "file\t use file");
    }

    public void eval (final String[] args) throws Exception { // main ... a sort of
        log = Logger.getGlobal();
        log.setLevel (__LOG_LEVEL);
        // if (args.length < 1) return;
        // int[][] 任务 = read_input (args[1]);
        int 荐, disp, gran;
        boolean show;
        try {
            荐 = args.length < 3 ? 4 : Integer.parseInt (args[2]);
            disp = args.length < 4 ? (荐 * 5 / 4) : Integer.parseInt (args[3]);
            gran = args.length < 5 ? (荐 / 8) : Integer.parseInt (args[4]);
            if (gran < 1)
                gran = 1;
            show = args.length < 6 ? false : args[5].equals("t");
        }
        catch (Exception ex) {
            荐 = 4;
            disp = 1;
            gran = 1;
            show = true;
        }
        try {
            InputData 任务 = read_parse_input (args); // rènwù
            System.out.println (process1 (任务, 荐, disp, gran, show));
            System.out.println (process2 (任务, 荐, disp, gran, show));
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private Scanner get_input_scanner (final String 源) throws IOException {
        /**
         * helper for getting correct instance of Scanner for
         * reading lines from:
         * stdin (源="-")
         * or other file
         */
         return 源.equals("-") ?
            new Scanner (System.in) :
            new Scanner (FileSystems.getDefault().getPath (源));
    }

    private String[] read_lines (final String 源) throws IOException {
        /**
         * read lines from stdin (源="-") or other file
         */
        Scanner 输入流 = get_input_scanner (源); // shūrù liú
        ArrayList <String> 矩阵 = new ArrayList <String> (); // jǔzhèn
        for (String 串; 输入流.hasNext();) {
            串 = 输入流.nextLine();
            if (串 != null && 串.length() > 0)
                矩阵.add (串);
        }
        return 矩阵.toArray (new String [矩阵.size()]);
    }

    private InputData read_parse_input (final String[] args)
        throws IOException {
        String[] 串川 = null;
        if (args.length < 2)
            串川 = test_input;
        else {
            if (args[1].equals ("test"))
                串川 = test_input;
            else
                if (args[1].equals ("builtin")) {
                    //串川 = data_input;
                    System.out.println ("no builtin data");
                    System.exit (1);
                }
                else
                    串川 = read_lines (args[1].equals ("-") ? "-" : args[1]);
        }
        return parse_input (串川);
    }

    private InputData parse_input (final String[] 任务) {
        InputData 桂马 = new InputData (任务); // gui ma
        ArrayList <ドト> 天点 = new ArrayList <> (任务.length);
        for (String 串 : 任务)
            try {
                天点.add (new ドト (串));
            }
            catch (Exception ex) {
                log.info ("wrong line : " + 串);
            }
        if (天点.size() < 1)
            return null;
        桂马.光点 = 天点.toArray (new ドト[0]);
        return 桂马;
    }

    private String process1 (final InputData 任务, final int 荐, int disp, int gran, boolean show) { // jiàn
        /**
         * Гипотезы:
         * 1. сообщение проявляется при компактном размещении точек;
         * 1.1. т.е., минимум дисперсии или около того;
         * 1.2. нужно посчитать центр масс;
         */
        int 步;
        double 标, 中;
        for (步 = 1; 步 <= 荐 ;++步) {
            one_step (任务);
            average (任务);
            dispersion (任务);
            //System.out.printf ("%d : %e %e\t %e %e\n", 步, 任务.东中, 任务.标准差东, 任务.南中, 任务.标准差南);
            if (步 > disp && (gran == 1 || 步 % gran == 1)) {
                if (show) {
                    System.out.printf ("\n\tStep %d\n", 步);
                    print(任务);
                }
                System.out.printf ("%d : %f\t %f\n", 步,
                                   任务.标准差东,
                                   任务.标准差南);
            }
        }
        return "done!";
    }

    private String process2 (final InputData 任务, final int 荐, int disp, int gran, boolean show) {
        return "done!";
    }

    private void one_step (final InputData 任务) {
        for (ドト 点 : 任务.光点) {
            点.东 += 点.速东;
            点.南 += 点.速南;
        }
    }

    private void print (final InputData 任务) {
        int 东元, 南元, 东尾, 南尾, 行, 列, 长; // wěi
        东元 = 南元 = 0x7FFFF;
        东尾 = 南尾 = -0x7FFF;
        for (ドト 点 : 任务.光点) {
            if (点.东 < 东元)
                东元 = 点.东;
            if (点.东 > 东尾)
                东尾 = 点.东;
            if (点.南 < 南元)
                南元 = 点.南;
            if (点.南 > 南尾)
                南尾 = 点.南;
        }
        长 = 东尾 - 东元 + 1;
        System.out.printf ("%d : %d\n", 长, 南尾 - 南元 + 1);
        boolean[][] 格 = new boolean[南尾 - 南元 + 1][];
        for (行 = 0; 行 < 格.length; ++行) {
            格[行] = new boolean [长];
            Arrays.fill (格[行], false);
        }
        for (ドト 点 : 任务.光点) {
            行 = 点.南 - 南元;
            列 = 点.东 - 东元;
            格[行][列] = true;
        }
        StringBuilder 串;
        for (行 = 0; 行 <= (南尾 - 南元); ++行) {
            串 = new StringBuilder (格[行].length);
            for (列 = 0; 列 < 格[行].length; ++列)
                串.append (格[行][列] ? '#' : '.');
            System.out.println (串);
        }
    }

    private double average (InputData 任务) {
        任务.南中 = 0;
        任务.东中 = 0;
        for (ドト 点 : 任务.光点) {
            任务.南中 += 点.南;
            任务.东中 += 点.东;
        }
        任务.南中 /= 任务.光点.length;
        任务.东中 /= 任务.光点.length;
        return (任务.南中 + 任务.东中) / 2;
    }

    private double dispersion (InputData 任务) {
        double 南数 = 0, 东数 = 0, 号;
        for (ドト 点 : 任务.光点) {
            号 = 点.南 - 任务.南中;
            南数 += 号 * 号;
            号 = 点.东 - 任务.东中;
            东数 += 号 * 号;
        }
        南数 /= 任务.光点.length;
        东数 /= 任务.光点.length;
        任务.标准差南 = 南数;
        任务.标准差东 = 东数;
        return (南数 + 东数) / 2;
    }

    class InputData {
        String[] 表;
        ドト[] 光点; // guāng
        double 南中, 东中, // center of masses of colud of dots
            标准差南, 标准差东; // biāo zhǔn chā - SD's
        InputData() {表 = null;}
        InputData (String[] 目录) {表 = Arrays.copyOf (目录, 目录.length);}
        public String toString() {return Arrays.toString (表);}
    }

    final static Pattern 路 = Pattern.compile ("^position=<\\s*(-?\\d+),\\s*(-?\\d+)> velocity=<\\s*(-?\\d+),\\s*(-?\\d+)>$");
    class ドト {
        int 南, 东; // (initial) position
        final int 速南, 速东; // velocity

        /**
         * position=< 9,  1> velocity=< 0,  2>
         * position=<-2,  7> velocity=< 2, -2>
         */
        ドト (String 串) {
            Matcher 配 = 路.matcher (串);
            //System.out.println (串);
            //System.out.println (配.matches());
            if (配.matches()) {
                东 = Integer.parseInt (配.group (1));
                南 = Integer.parseInt (配.group (2));
                速东 = Integer.parseInt (配.group (3));
                速南 = Integer.parseInt (配.group (4));
            }
            else
                throw new IllegalArgumentException ("cant parse : " + 串);
        }

        public String toString() {
            return String.format ("from (%d,%d) with (%d,%d)", 东, 南, 速东, 速南);
        }
    }

}
