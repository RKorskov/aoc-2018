-*- mode: markdown; coding: utf-8; indent-tabs-mode: nil; word-wrap: t -*-  
-*- eval: (set-language-environment Russian) -*-  
--- Time-stamp: <2018-12-04 23:38:04 roukoru>

# AoC-2018

[https://adventofcode.com/2018]

# Naming conventions

- 变数 (local) variable name
- クラッス class name
- めとで class's method name
- anything other
