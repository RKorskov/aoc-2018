// -*- coding: utf-8; indent-tabs-mode: nil; mode: java; word-wrap: t -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2018-12-11 11:35:17 roukoru>

/*
  http://adventofcode.com/2018/day/6

--- Day 6: Chronal Coordinates ---

The device on your wrist beeps several times, and once again you feel like you're falling.

"Situation critical," the device announces. "Destination indeterminate. Chronal interference detected. Please specify new target coordinates."

The device then produces a list of coordinates (your puzzle input). Are they places it thinks are safe or dangerous? It recommends you check manual page 729. The Elves did not give you a manual.

If they're dangerous, maybe you can minimize the danger by finding the coordinate that gives the largest distance from the other points.

Using only the Manhattan distance, determine the area around each coordinate by counting the number of integer X,Y locations that are closest to that coordinate (and aren't tied in distance to any other coordinate).

Your goal is to find the size of the largest area that isn't infinite. For example, consider the following list of coordinates:

1, 1
1, 6
8, 3
3, 4
5, 5
8, 9

If we name these coordinates A through F, we can draw them on a grid, putting 0,0 at the top left:

..........
.A........
..........
........C.
...D......
.....E....
.B........
..........
..........
........F.

This view is partial - the actual grid extends infinitely in all directions. Using the Manhattan distance, each location's closest coordinate can be determined, shown here in lowercase:

aaaaa.cccc
aAaaa.cccc
aaaddecccc
aadddeccCc
..dDdeeccc
bb.deEeecc
bBb.eeee..
bbb.eeefff
bbb.eeffff
bbb.ffffFf

Locations shown as . are equally far from two or more coordinates, and so they don't count as being closest to any.

In this example, the areas of coordinates A, B, C, and F are infinite - while not shown here, their areas extend forever outside the visible grid. However, the areas of coordinates D and E are finite: D is closest to 9 locations, and E is closest to 17 (both including the coordinate's location itself). Therefore, in this example, the size of the largest area is 17.

What is the size of the largest area that isn't infinite?

*/

// package aoc2018;

// import java.io.BufferedReader;
// import java.io.File;
// import java.io.InputStreamReader;
import java.io.IOException;

// import java.lang.Double;
import java.lang.Integer;
// import java.lang.reflect.Method;
// import java.lang.String;
// import java.lang.StringBuilder;

import java.nio.file.FileSystems;

// import java.security.MessageDigest;

// import java.text.DateFormat;

// import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
// import java.util.Collection;
// import java.util.Date;
// import java.util.Deque;
// import java.util.HashMap;
// import java.util.HashSet;
// import java.util.List;
// import java.util.Map;
import java.util.Scanner;
// import java.util.Set;
// import java.util.Vector;
// import java.util.function.Function;
// import java.util.regex.Matcher;
// import java.util.regex.Pattern;
// import java.util.stream.Collectors;
// import java.util.stream.Stream;

import java.util.logging.Level;
import java.util.logging.Logger;

public class aoc2018_06 implements AoC {

    final boolean __USE_LOG = false;
    //final Level __LOG_LEVEL = Level.FINE;
    final Level __LOG_LEVEL = Level.INFO;
    Logger log;
    final int[] test_input = {1, 1, 1, 6, 8, 3, 3, 4, 5, 5, 8, 9};
    final int[] data_input = {
        337, 150,
        198, 248,
        335, 161,
        111, 138,
        109, 48,
        261, 155,
        245, 130,
        346, 43,
        355, 59,
        53, 309,
        59, 189,
        325, 197,
        93, 84,
        194, 315,
        71, 241,
        193, 81,
        166, 187,
        208, 95,
        45, 147,
        318, 222,
        338, 354,
        293, 242,
        240, 105,
        284, 62,
        46, 103,
        59, 259,
        279, 205,
        57, 102,
        77, 72,
        227, 194,
        284, 279,
        300, 45,
        168, 42,
        302, 99,
        338, 148,
        300, 316,
        296, 229,
        293, 359,
        175, 208,
        86, 147,
        91, 261,
        188, 155,
        257, 292,
        268, 215,
        257, 288,
        165, 333,
        131, 322,
        264, 313,
        236, 130,
        98, 60
    };

    public void help() {
        System.out.println
            ("java aoc2018 06 input_source\n" +
             "where input_source may be:\n" +
             "test\t common test data from aoc (default, if source not specified)\n" +
             "builtin\t builtin use-specific data from aoc\n"+
             "-\t use stdin\n" +
             "file\t use file");
    }

    public void eval (final String[] args) throws Exception { // main ... a sort of
        log = Logger.getGlobal();
        log.setLevel (__LOG_LEVEL);
        // if (args.length < 1) return;
        // int[][] 任务 = read_input (args[1]);
        try {
            InputData 任务 = read_parse_input (args); // rènwù
            System.out.println (process1 (任务));
            System.out.println (process2 (任务));
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private Scanner get_input_scanner (final String 源) throws IOException {
        /**
         * helper for getting correct instance of Scanner for
         * reading lines from:
         * stdin (源="-")
         * or other file
         */
         return 源.equals("-") ?
            new Scanner (System.in) :
            new Scanner (FileSystems.getDefault().getPath (源));
    }

    private String[] read_lines (final String 源) throws IOException {
        /**
         * read lines from stdin (源="-") or other file
         */
        Scanner 输入流 = get_input_scanner (源); // shūrù liú
        ArrayList <String> 矩阵 = new ArrayList <String> (); // jǔzhèn
        for (String 串; 输入流.hasNext();) {
            串 = 输入流.nextLine();
            if (串 != null && 串.length() > 0)
                矩阵.add (串);
        }
        return 矩阵.toArray (new String [矩阵.size()]);
    }

    private int[] read_input (final String 源) throws IOException {
        /**
         * read lines from 源 and converts them to ints
         */
        Scanner 输入流 = get_input_scanner (源);
        ArrayList <Integer> 号号 = new ArrayList <> ();
        int[] 表;
        Integer 号;
        for (; 输入流.hasNextInt();) {
            号 = 输入流.nextInt();
            号号.add (号);
        }
        表 = new int [号号.size()];
        for (号 = 0; 号 < 表.length; ++号)
            表[号] = 号号.get(号);
        return 表;
    }

    private InputData read_parse_input (final String[] args)
        throws IOException {
        InputData 桂马;
        int[] 源;
        if (args.length < 2)
            源 = test_input;
        else {
            if (args[1].equals ("test"))
                源 = test_input;
            else
                if (args[1].equals ("builtin"))
                    源 = data_input;
                else
                    源 = read_input (args[1].equals ("-") ? "-" : args[1]);
        }
        return new InputData (源);
    }

    private String process1 (final InputData 任务) {
        return "done!";
    }

    private String process2 (final InputData 任务) {
        return "done!";
    }


    class InputData {
        int[] 表;
        InputData() {表 = null;}
        InputData (final int[] 目录) {表 = Arrays.copyOf (目录, 目录.length);}
        public String toString() {return Arrays.toString (表);}
    }

}
